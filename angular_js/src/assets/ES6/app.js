(function() {
	// init
  angular.module('App', ['ui.router'])

    // service
    require("./service/http.js")

    // Directives
    require("./directives/dir.js")
    require("./directives/page_title.js")

    // Router
    require("./router/router.js")

  
  })();
