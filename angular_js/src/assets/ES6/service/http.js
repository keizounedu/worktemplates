(function() {
  'use strict';
  class http {
    /* @ngInject */
    constructor($http,$httpParamSerializerJQLike) {
      this.$http = $http;
      this.$httpParamSerializerJQLike = $httpParamSerializerJQLike;
    }
    execute(option) {
      return this.$http(option)
    }
  }
  angular.module('App')
  .factory('http', ($http,$httpParamSerializerJQLike) => new http($http,$httpParamSerializerJQLike));
})()

// $http({
//     method: 'POST',
//     url: '/app/media/upload/',
//     transformRequest: null,
//     headers: {
//         'Content-Type': undefined
//     },
//     data: formData
// }).success(function(response) {
//     that.result = response;
// }).error(function(response) {
//     that.result = "error";
// })
