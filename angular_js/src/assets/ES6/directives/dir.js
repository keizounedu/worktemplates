
// Template Load
var dir_tpl = require("../views/dir.js").tpl

// Controller Load
var dir_ctrl = require("../controllers/dir.js").ctrl

angular.module('App').component('dirDirective',{
  bindings: {
    name: "="
  },
  controllerAs: 'dir',
  template: dir_tpl,
  controller: dir_ctrl
});

