angular.module('App').directive('pageTitle', function($rootScope) {
  return {
    link: function(scope, element) {
      var listener = function(event, state) {
        var pageTitle = '';
        if (state.title) {
          pageTitle = state.title + ' | Keizou nedu';
          element.text(pageTitle);
        }
      };
      $rootScope.$on('$stateChangeSuccess', listener);
    }
  };
});