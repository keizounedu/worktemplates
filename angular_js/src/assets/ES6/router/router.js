angular.module('App').config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
  .state('root', {
    url: "",
    template: "<dir-directive>",
    title: 'Top'
  })
  .state('dir', {
    url: "/dir",
    template: "<dir-directive>",
    title: 'Dir'
  })
});
