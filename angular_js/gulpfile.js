'use strict'

let gulp = require('gulp'),
    concat = require("gulp-concat"),
    plumber = require('gulp-plumber'),
    uglify = require("gulp-uglify"),
    rename = require('gulp-rename'),
    browser = require('browser-sync'),
    runSequence = require("run-sequence"),
    stylus = require("gulp-stylus"),
    csscomb = require("gulp-csscomb"),
    autoprefixer = require('gulp-autoprefixer'),
    browserify = require('browserify'),
    babelify = require('babelify'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    ngAnnotate = require('gulp-ng-annotate')


let CONFIG = {
    PC: {
        DEV: {
            PATH: {
                BASE: './src',
                STYLES: {
                    STYLUS: 'src/assets/Stylus',
                    DEST: 'src/assets/css',
                },
                SCRIPTS: {
                    ES6: 'src/assets/ES6',
                    LIBS: 'src/assets/js/libs',
                    DEST: 'src/assets/js'
                }
            }
        },
        DIST: {
            PATH: {
                STYLES: {
                    STYLUS: 'src/assets/Stylus',
                    DEST: 'public/assets/css',
                },
                SCRIPTS: {
                    ES6: 'src/assets/ES6',
                    LIBS: 'src/assets/js/libs',
                    DEST: 'public/assets/js',
                },
                IMG: {
                    URL: 'src/assets/img',
                    DEST: 'public/assets/img'
                }
            }
        }
    },
    SP: {
        DEV: {
            PATH: {
                BASE: './src',
                STYLES: {
                    STYLUS: 'src/sp/assets/Stylus',
                    DEST: 'src/sp/assets/css',
                },
                SCRIPTS: {
                    ES6: 'src/sp/assets/ES6',
                    LIBS: 'src/sp/assets/js/libs',
                    DEST: 'src/sp/assets/js'
                }
            }
        },
        DIST: {
            PATH: {
                STYLES: {
                    STYLUS: 'src/sp/assets/Stylus',
                    DEST: 'public/sp/assets/css',
                },
                SCRIPTS: {
                    ES6: 'src/sp/assets/ES6',
                    LIBS: 'src/sp/assets/js/libs',
                    DEST: 'public/sp/assets/js',
                },
                IMG: {
                    URL: 'src/sp/assets/img',
                    DEST: 'public/sp/assets/img'
                }
            }
        }
    }
}

/*
  ------------------------
  Select Mode
  ------------------------
*/
if (process.argv.slice(2)[2] != "") {
    switch (process.argv.slice(2)[2]) {
        case "PC":
            var ROOT = CONFIG.PC;
            break;
        case "SP":
            var ROOT = CONFIG.SP;
            break;
    }
}

/*
  ------------------------
  Develop Mode
  ------------------------
*/
gulp.task('dev_stylus', () => {
    gulp.src([ROOT.DEV.PATH.STYLES.STYLUS + '/*.styl'])
        .pipe(plumber())
        .pipe(stylus({
            'include css': true
        }))
        .pipe(autoprefixer())
        .pipe(gulp.dest(ROOT.DEV.PATH.STYLES.DEST))
        .pipe(csscomb())
        .pipe(gulp.dest(ROOT.DEV.PATH.STYLES.DEST))
        .pipe(browser.stream());
});
gulp.task("dev_script", () => {
     browserify([ROOT.DEV.PATH.SCRIPTS.ES6 + '/app.js'], { debug: false })
    .transform(
        babelify, {presets: ["es2015"]}
    )
    .bundle()
    .on("error", function (err) { console.log("Error : " + err.message); })
    .pipe(source('app.js'))
    .pipe(gulp.dest(ROOT.DEV.PATH.SCRIPTS.DEST))
    .pipe(rename('app.min.js'))
    .pipe(buffer())
    .pipe(ngAnnotate())
    .pipe(uglify())
    .pipe(gulp.dest(ROOT.DEV.PATH.SCRIPTS.DEST))
    .pipe(browser.stream());
    gulp.src(ROOT.DEV.PATH.SCRIPTS.LIBS + '/*.js')
        .pipe(concat('bundle.js'))
        .pipe(gulp.dest(ROOT.DEV.PATH.SCRIPTS.DEST+'/vendor/'))
        .pipe(browser.stream());
});
gulp.task("dev_web_server", () => {
    browser({
        server: {
            baseDir: ROOT.DEV.PATH.BASE
        }
    });
});
gulp.task("dev", ['dev_web_server'], () => {
    gulp.watch([ROOT.DEV.PATH.SCRIPTS.ES6 + '/*.js',ROOT.DEV.PATH.SCRIPTS.LIBS + '/*.js'], ["dev_script"]);
    gulp.watch([ROOT.DEV.PATH.STYLES.STYLUS + '/*.styl'], ["dev_stylus"]);
});

/*
  ------------------------
  Build Mode
  ------------------------
*/
const BUILD_HTML = ["./src/*.html", "./src/**/*.html", "./src/*.htm", "./src/**/*.htm", "./src/*.php", "./src/**/*.php", "./src/*.php", "./src/**/*.php"],
      BUILD_INCLUDE_HTML = ["./src/assets/include/*html", "./src/assets/include/*.php"]
gulp.task('build_html', () => {
    gulp.src(BUILD_HTML)
        .pipe(plumber())
        .pipe(gulp.dest('./public/'))
    gulp.src(BUILD_INCLUDE_HTML)
        .pipe(plumber())
        .pipe(gulp.dest('public/assets/include/'))
});
gulp.task('build_image', () => {
    gulp.src(ROOT.DIST.PATH.IMG.URL + "/**/")
        .pipe(gulp.dest(ROOT.DIST.PATH.IMG.DEST));
});
gulp.task('build_stylus', () => {
    gulp.src([ROOT.DIST.PATH.STYLES.STYLUS + '/*.styl'])
        .pipe(plumber())
        .pipe(stylus({
            'include css': true
        }))
        .pipe(autoprefixer())
        .pipe(gulp.dest(ROOT.DIST.PATH.STYLES.DEST))
        .pipe(csscomb())
        .pipe(gulp.dest(ROOT.DIST.PATH.STYLES.DEST))
        .pipe(browser.stream());
});
gulp.task("build_script", () => {

    browserify([ROOT.DEV.PATH.SCRIPTS.ES6 + '/app.js'], { debug: false })
    .transform(
        babelify, {presets: ["es2015"]}
    )
    .bundle()
    .on("error", function (err) { console.log("Error : " + err.message); })
    .pipe(source('app.js'))
    .pipe(gulp.dest(ROOT.DIST.PATH.SCRIPTS.DEST))
    .pipe(rename('app.min.js'))
    .pipe(buffer())
    .pipe(ngAnnotate())
    .pipe(uglify())
    .pipe(gulp.dest(ROOT.DIST.PATH.SCRIPTS.DEST))
    .pipe(browser.stream());
    gulp.src(ROOT.DIST.PATH.SCRIPTS.LIBS + '/*.js')
        .pipe(concat('bundle.js'))
        .pipe(gulp.dest(ROOT.DIST.PATH.SCRIPTS.DEST + '/vendor/'))
        .pipe(browser.stream());
});
gulp.task('build', (cb) => {
    runSequence('build_stylus','build_script','build_html','build_image')
})

gulp.task("check", ['dist_server'], () => {});

// gulp.task("dist", ['deploy__stylus', 'deploy__script', 'deploy__html', 'deploy__imgCopy']);
