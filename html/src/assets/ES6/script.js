(function(root) {

  class Template {

    constructor() {
      this.cacheElements();
      this.bindEvents();
      this.build();
    }

    cacheElements() {}

    bindEvents() {}

    build() {}

  }

  root.PROJECT = root.PROJECT || {}
  root.PROJECT.Template = Template || {}

  root.addEventListener("DOMContentLoaded", function(){
    new root.PROJECT.Template();
  }, false);

}(window))
