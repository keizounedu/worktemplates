'use strict'

let gulp = require('gulp'),
browser = require('browser-sync'),
rimraf = require('rimraf'),
babel = require('gulp-babel'),
plumber = require('gulp-plumber'),
concat = require('gulp-concat'),
csscomb = require('gulp-csscomb'),
stylus = require('gulp-stylus'),
uglify = require('gulp-uglify'),
sourcemaps = require('gulp-sourcemaps'),
autoprefixer = require("gulp-autoprefixer"),
runSequence = require("run-sequence")

/*
------------------------
Config File
------------------------
*/
let CONFIG = {
  DEV: {
    PATH: {
      BASE: './src',
      STYLES: {
        STYLUS: 'src/assets/Stylus',
        DEST: 'src/assets/css',
      },
      SCRIPTS: {
        ES6: 'src/assets/ES6',
        LIBS: 'src/assets/js/libs',
        DEST: 'src/assets/js'
      }
    }
  },
  DIST: {
    PATH: {
      STYLES: {
        STYLUS: 'src/assets/Stylus',
        DEST: 'public/assets/css',
      },
      SCRIPTS: {
        ES6: 'src/assets/ES6',
        LIBS: 'src/assets/js/libs',
        DEST: 'public/assets/js',
      },
      IMG: {
        URL: 'src/assets/img',
        DEST: 'public/assets/img'
      }
    }
  }
}


/*
------------------------
Project Develop
------------------------
*/
gulp.task('dev_stylus', () => {
  gulp.src([CONFIG.DEV.PATH.STYLES.STYLUS + '/*.styl'])
  .pipe(plumber())
  .pipe(stylus({
    'include css': true
  }))
  .pipe(autoprefixer())
  .pipe(gulp.dest(CONFIG.DEV.PATH.STYLES.DEST))
  .pipe(csscomb())
  .pipe(gulp.dest(CONFIG.DEV.PATH.STYLES.DEST))
  .pipe(browser.stream());
});
gulp.task("dev_script", () => {
  gulp.src([CONFIG.DEV.PATH.SCRIPTS.ES6 + '/*.js'])
  .pipe(plumber())
  .pipe(sourcemaps.init())
  .pipe(babel({
    presets: ['es2015-loose']
  }))
  .pipe(uglify())
  .pipe(sourcemaps.write())
  .pipe(gulp.dest(CONFIG.DEV.PATH.SCRIPTS.DEST))
  .pipe(browser.stream());
  gulp.src(CONFIG.DEV.PATH.SCRIPTS.LIBS + '/*.js')
  .pipe(concat('bundle.js'))
  .pipe(gulp.dest(CONFIG.DEV.PATH.SCRIPTS.DEST + '/vendor/'))
  .pipe(browser.stream());
});
gulp.task("dev_web_server", () => {
  browser({
    server: {
      baseDir: CONFIG.DEV.PATH.BASE
    }
  });
});
gulp.task('dev', ['dev_web_server'], () => {
  gulp.watch([CONFIG.DEV.PATH.SCRIPTS.ES6 + '/*.js'], ["dev_script"]);
  gulp.watch([CONFIG.DEV.PATH.STYLES.STYLUS + '/*.styl'], ["dev_stylus"]);
})


/*
------------------------
Project Build
------------------------
*/
const BUILD_HTML = ["./src/*.html", "./src/**/*.html", "./src/*.htm", "./src/**/*.htm", "./src/*.php", "./src/**/*.php", "./src/*.php", "./src/**/*.php"],
BUILD_INCLUDE_HTML = ["./src/assets/include/*html", "./src/assets/include/*.php"]
gulp.task('build_html', () => {
  gulp.src(BUILD_HTML)
  .pipe(plumber())
  .pipe(gulp.dest('./public/'))
  gulp.src(BUILD_INCLUDE_HTML)
  .pipe(plumber())
  .pipe(gulp.dest('public/assets/include/'))
});
gulp.task('build_image', () => {
  gulp.src(CONFIG.DIST.PATH.IMG.URL + "/**/")
  .pipe(gulp.dest(CONFIG.DIST.PATH.IMG.DEST));
});
gulp.task('build_stylus', () => {
  gulp.src([CONFIG.DIST.PATH.STYLES.STYLUS + '/*.styl'])
  .pipe(plumber())
  .pipe(stylus({
    'include css': true
  }))
  .pipe(autoprefixer())
  .pipe(gulp.dest(CONFIG.DIST.PATH.STYLES.DEST))
  .pipe(csscomb())
  .pipe(gulp.dest(CONFIG.DIST.PATH.STYLES.DEST))
  .pipe(browser.stream());
});
gulp.task("build_script", () => {
  gulp.src([CONFIG.DIST.PATH.SCRIPTS.ES6 + '/*.js'])
  .pipe(plumber())
  .pipe(sourcemaps.init())
  .pipe(babel({
    presets: ['es2015-loose']
  }))
  .pipe(uglify())
  .pipe(sourcemaps.write())
  .pipe(gulp.dest(CONFIG.DIST.PATH.SCRIPTS.DEST))
  .pipe(browser.stream());
  gulp.src(CONFIG.DIST.PATH.SCRIPTS.LIBS + '/*.js')
  .pipe(concat('bundle.js'))
  .pipe(gulp.dest(CONFIG.DIST.PATH.SCRIPTS.DEST + '/vendor/'))
  .pipe(browser.stream());
});
gulp.task('clean', function (cb) {
  rimraf('./public', cb);
});
gulp.task('build', (cb) => {
  runSequence('clean','build_stylus','build_script','build_html','build_image')
})
gulp.task("build_web_server", () => {
  browser({
    server: {
      baseDir: CONFIG.DIST.PATH.BASE
    }
  });
});
