const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const autoprefixer = require('autoprefixer');
const env = process.env.NODE_ENV;

module.exports = [
  // JS
  {
    context: __dirname + '/src',
    entry: {
      'app': './assets/_ES2015/app',
    },
    output: {
      path: __dirname + '/src/assets/js',
      filename: '[name].js'
    },
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
      new webpack.optimize.UglifyJsPlugin()
    ],
    module: {
      loaders: [
        { 
          test: /\.js$/, 
          exclude: /node_modules/, 
          loader: "babel", 
          query:{
            presets: ['es2015']
          }
        }
      ]
    }
  },
  {
    context: __dirname + '/src',
    entry: {
      'style': './assets/_STYLUS/style.styl'
    },
    output: {
      path: __dirname + '/src/assets/css',
      filename: '[name].css'
    },
    module: {
      loaders: [
        { 
          test: /\.styl$/, 
          loader:ExtractTextPlugin.extract('style-loader','css-loader?sourceMap!postcss-loader!stylus-loader')
        }
      ]
    },
    postcss: [ autoprefixer( { browsers: ['IE 9', 'IE 10', 'IE 11', 'last 2 versions'] } )],
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
      new ExtractTextPlugin('./[name].css'),
      new webpack.DefinePlugin({
          'process.env':{
              'NODE_ENV': JSON.stringify('production')
          }
      }),
      new webpack.optimize.UglifyJsPlugin({
          compress:{
              warnings: true
          }
      })
    ]

  }
]
